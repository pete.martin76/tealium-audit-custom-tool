{
  "id": "tag_audit_pete",
  "title": "Tag Audit_pete",
  "description": "",
  "scripts": {
    "_tagAudit": {
     "title": "Petes Tag Audit",  
     "auto_inject": true,
     "js": "https://glcdn.githack.com/pete.martin76/tealium-audit-custom-tool/raw/master/tealiumAuditPageCode.js",
     "template": "https://gitlab.com/pete.martin76/tealium-audit-custom-tool/raw/master/ui.html",
     "remote_js": true,
     "remote_template": true
    }
  }
}